FROM python:latest
WORKDIR /usr/app/src
COPY ./requirements.txt /tmp/
RUN pip3 install --no-cache-dir -r /tmp/requirements.txt
COPY ./src/ .
CMD ["python3", "server.py"]
