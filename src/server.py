#!/usr/bin/python3
#https://github.com/bazelbuild/bazel/issues/7091
#https://github.com/bazelbuild/rules_python/issues/382
import sys
if sys.path[0].startswith('/home/thingdust/src'):
    sys.path = sys.path[1:]

import asyncio
from widget import run_widget
from typing import Callable, Awaitable
from enum import Enum
import aiohttp
import aiohttp.client_exceptions
from aiohttp import web
import os
from dataclasses import dataclass
import json
import math
from typing import Union


def get_doc():
    return """
    ## set target to fly to

    ```shell
    $ curl -XPOST http://{IP_AND_PORT}/set_target --data '{"target": "Core Station"}'
    {"kind": "success"}

    $ curl -XPOST http://{IP_AND_PORT}/set_target --data '{"target": "station 1"}'
    {"kind": "success"}

    $ curl -XPOST http://{IP_AND_PORT}/set_target --data '{"target": "Vesta Station"}'
    {"kind": "success"}

    $ curl -XPOST http://{IP_AND_PORT}/set_target --data '{"target": {"x": 100, "y": 100}}'
    {"kind": "success"}
    ```
    """.replace('{IP_AND_PORT}', f'"192.168.0.3":2009')


thruster_ports = {
    'back': 2003,
    'front': 2004,
    'top_left': 2005,
    'top_right': 2006,
    'bottom_left': 2007,
    'bottom_right': 2008,
}


class PidController:
    dt = 0.0
    max = 0.0
    min = 0.0
    kp = 0.0
    kd = 0.0
    ki = 0.0
    err = 0.0
    int = 0.0

    def __init__(self, dt, max, min, kp, kd, ki):
        self.dt = dt
        self.max = max
        self.min = min
        self.kp = kp
        self.kd = kd
        self.ki = ki

    def run(self, set, act):
        error = set - act

        P = self.kp * error

        self.int += error * self.dt
        I = self.ki * self.int

        D = self.kd * (error - self.err) / self.dt

        output = P + I + D

        if output > self.max:
            output = self.max
        elif output < self.min:
            output = self.min

        self.err = error
        return output


async def set_thruster(name: str, strength: float):
    async with aiohttp.ClientSession() as session:
        async with session.put(
            f'http://{os.environ["K8S_HOST"]}:{thruster_ports[name]}/thruster',
            json={'thrust_percent': float(max(0, min(100, strength * 100)))}
        ) as response:
            assert (await response.json())['kind'] == 'success'


@dataclass
class Vec:
    x: float
    y: float
    angle: float


@dataclass
class Pos:
    pos: Vec
    velocity: Vec


class SpecialTarget(Enum):
    IDLE = 1
    STOP = 2


Target = Union[Vec, SpecialTarget]

current_target = SpecialTarget.IDLE


@dataclass
class Thrusters:
    back: float
    front: float
    top_left: float
    top_right: float
    bottom_left: float
    bottom_right: float


async def get_pos():
    async with aiohttp.ClientSession() as session:
        async with session.get(f'http://{os.environ["K8S_HOST"]}:2010/pos') as response:
            result = await response.json()
            assert result['kind'] == 'success'
            assert type(result['pos']['x']) == float
            assert type(result['pos']['y']) == float
            assert type(result['pos']['angle']) == float
            assert type(result['velocity']['x']) == float
            assert type(result['velocity']['y']) == float
            assert type(result['velocity']['angle']) == float
            return Pos(
                Vec(result['pos']['x'], result['pos']['y'], result['pos']['angle']),
                Vec(result['velocity']['x'], result['velocity']['y'], result['velocity']['angle']),
            )


async def set_thrusters(thrusters: Thrusters):
    await asyncio.gather(
        set_thruster('back', thrusters.back),
        set_thruster('front', thrusters.front),
        set_thruster('top_left', thrusters.top_left),
        set_thruster('top_right', thrusters.top_right),
        set_thruster('bottom_left', thrusters.bottom_left),
        set_thruster('bottom_right', thrusters.bottom_right),
    )


def set_rotate_throttle(thrusters: Thrusters, clockwise: float):
    if abs(clockwise) < 0.000001:
        thrusters.top_left = 0
        thrusters.top_right = 0
        thrusters.bottom_left = 0
        thrusters.bottom_right = 0
    elif clockwise > 0:
        thrusters.top_left = max(0.001, clockwise)
        thrusters.top_right = 0
        thrusters.bottom_left = 0
        thrusters.bottom_right = max(0.001, clockwise)
    else:
        thrusters.top_left = 0
        thrusters.top_right = max(0.001, -clockwise)
        thrusters.bottom_left = max(0.001, -clockwise)
        thrusters.bottom_right = 0


def set_forward_throttle(thrusters: Thrusters, strength: float):
    if abs(strength) < 0.000001:
        thrusters.back = 0
        thrusters.front = 0
    elif strength > 0:
        thrusters.back = max(0.001, strength)
        thrusters.front = 0
    else:
        thrusters.back = 0
        thrusters.front = max(0.001, -strength)


def set_x_throttle(thrusters: Thrusters, strength: float):
    limit = 0.2 #side thrusters are not synchronized: avoid unwanted rotate
    if abs(strength) < 0.00001:
        thrusters.top_left = 0
        thrusters.top_right = 0
        thrusters.bottom_left = 0
        thrusters.bottom_right = 0
    elif strength > 0:
        thrusters.top_left = min(limit, max(0.001, strength))
        thrusters.top_right = 0
        thrusters.bottom_left = min(limit, max(0.001, strength))
        thrusters.bottom_right = 0
    else:
        thrusters.top_left = 0
        thrusters.top_right = min(limit, max(0.001, -strength))
        thrusters.bottom_left = 0
        thrusters.bottom_right = min(limit, max(0.001, -strength))


def calc_diff_to_target(target_angle: float, current_angle: float) -> float:
    diff = current_angle - target_angle
    if diff > 180:
        diff -= 360
    if diff < -180:
        diff += 360
    return diff


async def angle_diff_to_target(target_angle):
    pos = (await get_pos()).pos
    return calc_diff_to_target(target_angle, pos.angle)


#TODO: implement proper PID controller instead of manual thingy here
async def logic_loop() -> Callable[[], Awaitable[None]]:
    pid_rotate = PidController(dt=1, max=180, min=-180, kp=5, kd=0, ki=0)
    pid_y = PidController(dt=1, max=1000, min=-1000, kp=200, kd=0, ki=0)

    def set_angle(thrusters: Thrusters, ship: Pos, target: float):
        diff = calc_diff_to_target(target, ship.pos.angle)
        pos_inc = pid_rotate.run(0, diff)
        print(f'p-angle target={(target%360): 6.2f} real={ship.pos.angle: 6.2f} inc={pos_inc: 6.2f} diff={diff: 6.2f}')
        if abs(diff) > 0.25 and abs(pos_inc) > 0.0001:
            set_rotate_throttle(thrusters, (pos_inc / pid_rotate.max))
            return False #angle needs to change first
        else:
            set_rotate_throttle(thrusters, 0)
            return abs(ship.velocity.angle) < 0.01 # velocity needs to be zero first

    def set_xy_pos(
        name: str,
        thrust_factor: float,
        thrusters: Thrusters,
        pid_pos: PidController,
        velocity: float,
        current: float,
        target: float,
        set_speed: Callable[[Thrusters, float], None],
    ):
        diff = target - current
        diff *= 0.01
        pos_inc = pid_pos.run(diff, velocity)
        print(
            f'p-{name} target={diff: 6.2f} current={velocity: 6.2f} thrust_factor={thrust_factor: 6.2f} -> inc={pos_inc: 6.2f}'
        )
        if abs(diff) > 0.1 and abs(pos_inc) > 0.0001:
            set_speed(thrusters, pos_inc / pid_pos.max)
            return False #pos needs to change first
        else:
            set_speed(thrusters, pos_inc / pid_pos.max)
            #set_speed(thrusters, 0)
            return abs(velocity) < 0.0001 # velocity needs to be zero first

    def step_go_to_target(ship: Pos, thrusters: Thrusters, target: Vec):
        dist_x = target.x - ship.pos.x
        dist_y = target.y - ship.pos.y
        dist = math.sqrt(dist_x * dist_x + dist_y * dist_y)
        #dist = dist_y * math.cos(math.radians(ship.pos.angle)) + dist_x * math.sin(math.radians(ship.pos.angle))
        velocity = ship.velocity.y * math.cos(math.radians(ship.pos.angle)
                                              ) + ship.velocity.x * math.sin(math.radians(ship.pos.angle))

        def stop_forward_speed():
            if abs(velocity) > 1:
                set_xy_pos(
                    'x',
                    1,
                    thrusters,
                    pid_pos=pid_y,
                    velocity=velocity,
                    current=0,
                    target=0,
                    set_speed=lambda thrusters, target_speed: set_forward_throttle(
                        thrusters,
                        target_speed * 0.25,
                    ),
                )

        if abs(dist) < 20 and abs(velocity) > 0.5:
            stop_forward_speed()
            return True
        elif abs(dist) > 20:
            target_angle = math.degrees(math.atan2(dist_x, dist_y))
            set_angle(thrusters, ship, target_angle)

            angle_dist = abs(calc_diff_to_target(target_angle, ship.pos.angle))
            thrust_factor = 0
            if abs(dist) > 2000:
                factor_table = [
                    (0.5, 1),
                    (1.0, 0.9),
                    (2.0, 0.8),
                    (3.0, 0.7),
                    (4.0, 0.5),
                    (8.0, 0.25),
                    (16.0, 0.125),
                ]
            else:
                factor_table = [
                    (0.1, 1),
                    (0.2, 0.9),
                    (0.4, 0.8),
                    (0.8, 0.7),
                    (1.6, 0.5),
                    (3.4, 0.25),
                    (6.8, 0.125),
                ]
            for max_angle_dist, factor in factor_table:
                if abs(angle_dist) < max_angle_dist:
                    thrust_factor = factor
                    break

            if abs(angle_dist) < 40:
                set_xy_pos(
                    'x',
                    thrust_factor,
                    thrusters,
                    pid_pos=pid_y,
                    velocity=velocity,
                    current=-dist * 3 * thrust_factor,
                    target=0,
                    set_speed=lambda thrusters, target_speed: set_forward_throttle(
                        thrusters,
                        target_speed,
                    ),
                )
                return False
            else:
                stop_forward_speed()
                return False
        return True

    def get_zero_thrusters():
        return Thrusters(
            back=0,
            front=0,
            top_left=0,
            top_right=0,
            bottom_left=0,
            bottom_right=0,
        )

    while True:
        match current_target:
            case SpecialTarget.IDLE:
                await asyncio.sleep(0.1)
                pass
            case SpecialTarget.STOP:
                await asyncio.sleep(0.1)
                #print()
                ship = await get_pos()

                thrusters = Thrusters(
                    back=0,
                    front=0,
                    top_left=0,
                    top_right=0,
                    bottom_left=0,
                    bottom_right=0,
                )

                if not set_angle(thrusters, ship, 0):
                    await set_thrusters(thrusters)
                    continue

                await set_thrusters(thrusters)
            case Vec(_, _, _) as v:
                ship = await get_pos()
                thrusters = get_zero_thrusters()
                #print()
                step_go_to_target(ship, thrusters, v)
                await set_thrusters(thrusters)
                await asyncio.sleep(0.1)

            case _:
                assert False, 'invalid state'


async def widget_loop(update_widget) -> Callable[[dict], Awaitable[None]]:
    while True:
        await update_widget(
            {
                'title': f'Easy Steering',
                'width': 2,
                'height': 1,
                'content': {
                    'kind':
                    'stack',
                    'content': [
                        {
                            'kind':
                            'text',
                            'text':
                            f'{current_target.x: 1.0f}/{current_target.y: 1.0f}'
                            if type(current_target) == Vec else f'{current_target}',
                        },
                        {
                            'kind': 'button',
                            'id': json.dumps({'action': 'idle'}),
                            'text': f'idle',
                            'pressed': current_target == SpecialTarget.IDLE
                        },
                        {
                            'kind': 'button',
                            'id': json.dumps({'action': 'stop'}),
                            'text': f'stop',
                            'pressed': current_target == SpecialTarget.STOP
                        },
                    ] + [
                        {
                            'kind': 'button',
                            'id': json.dumps({
                                'action': 'goto',
                                'name': name
                            }),
                            'text': name,
                            'pressed': current_target == coord,
                        } for name, coord in coords.items()
                    ]
                }
            }
        )
        await asyncio.sleep(0.5)


coords = {
    'Core Station': Vec(0, 0, 0),
    'Vesta Station': Vec(x=10000, y=10000, angle=0),
    'Azura Station': Vec(x=-1000, y=1000, angle=0),
    'Shangris Station': Vec(x=-23542, y=36158, angle=0),
    'Platin Mountain': Vec(x=49600, y=77618, angle=0),
    'Artemis Station': Vec(x=-21000, y=37792, angle=0),
    'Elyse Terminal': Vec(x=-9000, y=-7500, angle=0),
    'Illume Colony': Vec(x=666, y=666, angle=0),
    'Zurro Station': Vec(x=30000, y=-4000, angle=0),
    'Phantom Station': Vec(x=-60000, y=9000, angle=0),
    'Tiberius Station': Vec(x=-40000, y=-20000, angle=0),
    'Nemesis Station': Vec(x=11000, y=-11000, angle=0),
    'Research Station': Vec(x=18000, y=-20000, angle=0),
    'Architect Colony': Vec(x=-28795, y=52321, angle=0),
    'Aurora Station': Vec(x=-11000, y=-11000, angle=0)
}


async def read_widget(messages):
    global current_target
    async for message in messages:
        try:
            #print(message)
            match message['kind']:
                case 'button_pressed':
                    cmd = json.loads(message['id'])
                    action = cmd['action']
                    match action:
                        case 'idle':
                            current_target = SpecialTarget.IDLE
                        case 'stop':
                            current_target = SpecialTarget.STOP
                        case 'goto':
                            name = cmd['name']
                            current_target = coords[name]
                        case _:
                            assert False, 'unexpected button'
                case 'button_unpressed':
                    pass
                case _:
                    assert False, 'unexpected message'
        except KeyboardInterrupt:
            raise
        except Exception as ex:
            print(ex)


async def handleRoot(request):
    return web.Response(text=f'Welcome to easy steering')


async def handleSetTarget(request: web.Request):
    global current_target
    req = await request.json()
    match req['target']:
        case {'x': x, 'y': y}:
            current_target = Vec(x=x, y=y, angle=0)
        case name:
            if name in coords:
                current_target = coords[name]
    return web.json_response(text=json.dumps({"kind": "success"}))


async def retry_on_failure(fn):
    retries = 0
    while True:
        try:
            await fn()
            break
        except KeyboardInterrupt:
            raise
        except:
            retries += 1
            if retries > 10:
                raise
            await asyncio.sleep(1)


async def main() -> None:
    async with run_widget() as (messages, update_widget, send_command, update_doc):
        await update_doc(get_doc())
        app = web.Application()
        app.add_routes([
            web.get('/', handleRoot),
            web.post('/set_target', handleSetTarget),
        ])
        await asyncio.gather(
            web._run_app(app, port=2009),
            retry_on_failure(logic_loop),
            widget_loop(update_widget),
            read_widget(messages),
        )


if __name__ == '__main__':
    asyncio.run(main())
