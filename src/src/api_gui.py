#!/usr/bin/python3
import sys
import os
import abc
from decimal import Decimal
from dataclasses import dataclass
sys.path.append(os.path.join(os.path.dirname(os.path.realpath(__file__)), '..'))
from enum import Enum
from typing import NamedTuple, Optional, List, Union, Dict, AsyncGenerator

from api import gui_pb2, gui_grpc

class GrpcAccessDenied(Exception):
  pass


class ResourceType(Enum):
  GOLD = 0
  IRON = 1
  PLATIN = 2
  def to_pb(self):
    return self.value
  @staticmethod
  def from_pb(data):
    return ResourceType(data)
  def serialize(self) -> bytes:
    return self.to_pb().SerializeToString()
  @classmethod
  def deserialize(cls, data: bytes) -> 'ResourceType':
    obj = gui_pb2.ResourceType()
    obj.ParseFromString(data);
    return cls.from_pb(obj)
  
  
@dataclass
class Void:
  def to_pb(self):
    obj = gui_pb2.Void()
    return obj
  @staticmethod
  def from_pb(data) -> 'Void':
    return Void()
  def serialize(self) -> bytes:
    return self.to_pb().SerializeToString()
  @classmethod
  def deserialize(cls, data: bytes) -> 'Void':
    obj = gui_pb2.Void()
    obj.ParseFromString(data);
    return cls.from_pb(obj)


@dataclass
class Vec:
  x: 'float'
  y: 'float'
  angle: 'float'
  def to_pb(self):
    obj = gui_pb2.Vec()
    obj.x = self.x
    obj.y = self.y
    obj.angle = self.angle
    return obj
  @staticmethod
  def from_pb(data) -> 'Vec':
    return Vec(x = float("{:.7f}".format(data.x)),y = float("{:.7f}".format(data.y)),angle = float("{:.7f}".format(data.angle)),)
  def serialize(self) -> bytes:
    return self.to_pb().SerializeToString()
  @classmethod
  def deserialize(cls, data: bytes) -> 'Vec':
    obj = gui_pb2.Vec()
    obj.ParseFromString(data);
    return cls.from_pb(obj)


@dataclass
class PhysicalObject:
  pos: 'Vec'
  velocity: 'Vec'
  def to_pb(self):
    obj = gui_pb2.PhysicalObject()
    obj.pos.CopyFrom(self.pos.to_pb())
    obj.velocity.CopyFrom(self.velocity.to_pb())
    return obj
  @staticmethod
  def from_pb(data) -> 'PhysicalObject':
    return PhysicalObject(pos = Vec.from_pb(data.pos),velocity = Vec.from_pb(data.velocity),)
  def serialize(self) -> bytes:
    return self.to_pb().SerializeToString()
  @classmethod
  def deserialize(cls, data: bytes) -> 'PhysicalObject':
    obj = gui_pb2.PhysicalObject()
    obj.ParseFromString(data);
    return cls.from_pb(obj)


@dataclass
class Thruster:
  strength: 'float'
  def to_pb(self):
    obj = gui_pb2.Thruster()
    obj.strength = self.strength
    return obj
  @staticmethod
  def from_pb(data) -> 'Thruster':
    return Thruster(strength = float("{:.7f}".format(data.strength)),)
  def serialize(self) -> bytes:
    return self.to_pb().SerializeToString()
  @classmethod
  def deserialize(cls, data: bytes) -> 'Thruster':
    obj = gui_pb2.Thruster()
    obj.ParseFromString(data);
    return cls.from_pb(obj)


@dataclass
class ShipThrusters:
  back: 'Thruster'
  front: 'Thruster'
  front_left: 'Thruster'
  front_right: 'Thruster'
  bottom_left: 'Thruster'
  bottom_right: 'Thruster'
  def to_pb(self):
    obj = gui_pb2.ShipThrusters()
    obj.back.CopyFrom(self.back.to_pb())
    obj.front.CopyFrom(self.front.to_pb())
    obj.front_left.CopyFrom(self.front_left.to_pb())
    obj.front_right.CopyFrom(self.front_right.to_pb())
    obj.bottom_left.CopyFrom(self.bottom_left.to_pb())
    obj.bottom_right.CopyFrom(self.bottom_right.to_pb())
    return obj
  @staticmethod
  def from_pb(data) -> 'ShipThrusters':
    return ShipThrusters(back = Thruster.from_pb(data.back),front = Thruster.from_pb(data.front),front_left = Thruster.from_pb(data.front_left),front_right = Thruster.from_pb(data.front_right),bottom_left = Thruster.from_pb(data.bottom_left),bottom_right = Thruster.from_pb(data.bottom_right),)
  def serialize(self) -> bytes:
    return self.to_pb().SerializeToString()
  @classmethod
  def deserialize(cls, data: bytes) -> 'ShipThrusters':
    obj = gui_pb2.ShipThrusters()
    obj.ParseFromString(data);
    return cls.from_pb(obj)


@dataclass
class Ship:
  physical: 'PhysicalObject'
  thrusters: 'ShipThrusters'
  def to_pb(self):
    obj = gui_pb2.Ship()
    obj.physical.CopyFrom(self.physical.to_pb())
    obj.thrusters.CopyFrom(self.thrusters.to_pb())
    return obj
  @staticmethod
  def from_pb(data) -> 'Ship':
    return Ship(physical = PhysicalObject.from_pb(data.physical),thrusters = ShipThrusters.from_pb(data.thrusters),)
  def serialize(self) -> bytes:
    return self.to_pb().SerializeToString()
  @classmethod
  def deserialize(cls, data: bytes) -> 'Ship':
    obj = gui_pb2.Ship()
    obj.ParseFromString(data);
    return cls.from_pb(obj)


@dataclass
class Asteroid:
  physical: 'PhysicalObject'
  def to_pb(self):
    obj = gui_pb2.Asteroid()
    obj.physical.CopyFrom(self.physical.to_pb())
    return obj
  @staticmethod
  def from_pb(data) -> 'Asteroid':
    return Asteroid(physical = PhysicalObject.from_pb(data.physical),)
  def serialize(self) -> bytes:
    return self.to_pb().SerializeToString()
  @classmethod
  def deserialize(cls, data: bytes) -> 'Asteroid':
    obj = gui_pb2.Asteroid()
    obj.ParseFromString(data);
    return cls.from_pb(obj)


@dataclass
class MineableAsteroid:
  physical: 'PhysicalObject'
  resource: 'ResourceType'
  def to_pb(self):
    obj = gui_pb2.MineableAsteroid()
    obj.physical.CopyFrom(self.physical.to_pb())
    obj.resource = self.resource.to_pb()
    return obj
  @staticmethod
  def from_pb(data) -> 'MineableAsteroid':
    return MineableAsteroid(physical = PhysicalObject.from_pb(data.physical),resource = ResourceType.from_pb(data.resource),)
  def serialize(self) -> bytes:
    return self.to_pb().SerializeToString()
  @classmethod
  def deserialize(cls, data: bytes) -> 'MineableAsteroid':
    obj = gui_pb2.MineableAsteroid()
    obj.ParseFromString(data);
    return cls.from_pb(obj)


@dataclass
class SpaceStation:
  physical: 'PhysicalObject'
  in_reach_since: 'int'
  in_reach_since_with_laser: 'int'
  def to_pb(self):
    obj = gui_pb2.SpaceStation()
    obj.physical.CopyFrom(self.physical.to_pb())
    obj.in_reach_since = self.in_reach_since
    obj.in_reach_since_with_laser = self.in_reach_since_with_laser
    return obj
  @staticmethod
  def from_pb(data) -> 'SpaceStation':
    return SpaceStation(physical = PhysicalObject.from_pb(data.physical),in_reach_since = data.in_reach_since,in_reach_since_with_laser = data.in_reach_since_with_laser,)
  def serialize(self) -> bytes:
    return self.to_pb().SerializeToString()
  @classmethod
  def deserialize(cls, data: bytes) -> 'SpaceStation':
    obj = gui_pb2.SpaceStation()
    obj.ParseFromString(data);
    return cls.from_pb(obj)


@dataclass
class ResourceInHold:
  type: 'ResourceType'
  amount: 'int'
  def to_pb(self):
    obj = gui_pb2.ResourceInHold()
    obj.type = self.type.to_pb()
    obj.amount = self.amount
    return obj
  @staticmethod
  def from_pb(data) -> 'ResourceInHold':
    return ResourceInHold(type = ResourceType.from_pb(data.type),amount = data.amount,)
  def serialize(self) -> bytes:
    return self.to_pb().SerializeToString()
  @classmethod
  def deserialize(cls, data: bytes) -> 'ResourceInHold':
    obj = gui_pb2.ResourceInHold()
    obj.ParseFromString(data);
    return cls.from_pb(obj)


@dataclass
class ActiveMission:
  id: 'MissionId'
  step: 'int'
  def to_pb(self):
    obj = gui_pb2.ActiveMission()
    obj.id.CopyFrom(self.id.to_pb())
    obj.step = self.step
    return obj
  @staticmethod
  def from_pb(data) -> 'ActiveMission':
    return ActiveMission(id = MissionId.from_pb(data.id),step = data.step,)
  def serialize(self) -> bytes:
    return self.to_pb().SerializeToString()
  @classmethod
  def deserialize(cls, data: bytes) -> 'ActiveMission':
    obj = gui_pb2.ActiveMission()
    obj.ParseFromString(data);
    return cls.from_pb(obj)


@dataclass
class GameState:
  tick: 'int'
  ship: 'Ship'
  space_stations: Dict[str, 'SpaceStation']
  asteroids: List['Asteroid']
  mineable_asteroids: Dict[str, 'MineableAsteroid']
  credits: 'int'
  items: List['ItemId']
  resources: List['ResourceInHold']
  active_missions: List['ActiveMission']
  finished_missions: List['MissionId']
  current_artemis_target: 'int'
  missions_for_6: 'int'
  laser_is_active_since: 'int'
  laser_is_cooling_down_until: 'int'
  secret_key: 'str'
  laser_angle: 'float'
  laser_is_mining_since: 'int'
  laser_last_resource_collect: 'int'
  laser_is_enabled_since: 'int'
  def to_pb(self):
    obj = gui_pb2.GameState()
    obj.tick = self.tick
    obj.ship.CopyFrom(self.ship.to_pb())
    for key, value in self.space_stations.items():
      obj.space_stations.get_or_create(key).CopyFrom(value.to_pb())
    obj.asteroids.extend([element.to_pb() for element in self.asteroids])
    for key, value in self.mineable_asteroids.items():
      obj.mineable_asteroids.get_or_create(key).CopyFrom(value.to_pb())
    obj.credits = self.credits
    obj.items.extend([element.to_pb() for element in self.items])
    obj.resources.extend([element.to_pb() for element in self.resources])
    obj.active_missions.extend([element.to_pb() for element in self.active_missions])
    obj.finished_missions.extend([element.to_pb() for element in self.finished_missions])
    obj.current_artemis_target = self.current_artemis_target
    obj.missions_for_6 = self.missions_for_6
    obj.laser_is_active_since = self.laser_is_active_since
    obj.laser_is_cooling_down_until = self.laser_is_cooling_down_until
    obj.secret_key = self.secret_key
    obj.laser_angle = self.laser_angle
    obj.laser_is_mining_since = self.laser_is_mining_since
    obj.laser_last_resource_collect = self.laser_last_resource_collect
    obj.laser_is_enabled_since = self.laser_is_enabled_since
    return obj
  @staticmethod
  def from_pb(data) -> 'GameState':
    return GameState(tick = data.tick,ship = Ship.from_pb(data.ship),space_stations = {key: SpaceStation.from_pb(data.space_stations[key]) for key in data.space_stations},asteroids = [Asteroid.from_pb(element) for element in data.asteroids],mineable_asteroids = {key: MineableAsteroid.from_pb(data.mineable_asteroids[key]) for key in data.mineable_asteroids},credits = data.credits,items = [ItemId.from_pb(element) for element in data.items],resources = [ResourceInHold.from_pb(element) for element in data.resources],active_missions = [ActiveMission.from_pb(element) for element in data.active_missions],finished_missions = [MissionId.from_pb(element) for element in data.finished_missions],current_artemis_target = data.current_artemis_target,missions_for_6 = data.missions_for_6,laser_is_active_since = data.laser_is_active_since,laser_is_cooling_down_until = data.laser_is_cooling_down_until,secret_key = data.secret_key,laser_angle = float("{:.7f}".format(data.laser_angle)),laser_is_mining_since = data.laser_is_mining_since,laser_last_resource_collect = data.laser_last_resource_collect,laser_is_enabled_since = data.laser_is_enabled_since,)
  def serialize(self) -> bytes:
    return self.to_pb().SerializeToString()
  @classmethod
  def deserialize(cls, data: bytes) -> 'GameState':
    obj = gui_pb2.GameState()
    obj.ParseFromString(data);
    return cls.from_pb(obj)


@dataclass
class UiInfo:
  thruster_strength: 'float'
  forward_speed: 'float'
  jokers: 'int'
  cockpit_url: 'str'
  laser_length: 'float'
  def to_pb(self):
    obj = gui_pb2.UiInfo()
    obj.thruster_strength = self.thruster_strength
    obj.forward_speed = self.forward_speed
    obj.jokers = self.jokers
    obj.cockpit_url = self.cockpit_url
    obj.laser_length = self.laser_length
    return obj
  @staticmethod
  def from_pb(data) -> 'UiInfo':
    return UiInfo(thruster_strength = float("{:.7f}".format(data.thruster_strength)),forward_speed = float("{:.7f}".format(data.forward_speed)),jokers = data.jokers,cockpit_url = data.cockpit_url,laser_length = float("{:.7f}".format(data.laser_length)),)
  def serialize(self) -> bytes:
    return self.to_pb().SerializeToString()
  @classmethod
  def deserialize(cls, data: bytes) -> 'UiInfo':
    obj = gui_pb2.UiInfo()
    obj.ParseFromString(data);
    return cls.from_pb(obj)


@dataclass
class K8sState_TargetState:
  expected: 'int'
  ready: 'int'
  unknown: 'int'
  ports: List['int']
  def to_pb(self):
    obj = gui_pb2.K8sState.TargetState()
    obj.expected = self.expected
    obj.ready = self.ready
    obj.unknown = self.unknown
    obj.ports.extend([element for element in self.ports])
    return obj
  @staticmethod
  def from_pb(data) -> 'K8sState_TargetState':
    return K8sState_TargetState(expected = data.expected,ready = data.ready,unknown = data.unknown,ports = [element for element in data.ports],)
  def serialize(self) -> bytes:
    return self.to_pb().SerializeToString()
  @classmethod
  def deserialize(cls, data: bytes) -> 'K8sState_TargetState':
    obj = gui_pb2.TargetState()
    obj.ParseFromString(data);
    return cls.from_pb(obj)


@dataclass
class K8sState:
  targets: Dict[str, 'K8sState_TargetState']
  def to_pb(self):
    obj = gui_pb2.K8sState()
    for key, value in self.targets.items():
      obj.targets.get_or_create(key).CopyFrom(value.to_pb())
    return obj
  @staticmethod
  def from_pb(data) -> 'K8sState':
    return K8sState(targets = {key: K8sState_TargetState.from_pb(data.targets[key]) for key in data.targets},)
  def serialize(self) -> bytes:
    return self.to_pb().SerializeToString()
  @classmethod
  def deserialize(cls, data: bytes) -> 'K8sState':
    obj = gui_pb2.K8sState()
    obj.ParseFromString(data);
    return cls.from_pb(obj)


@dataclass
class StatusUpdate:
  state: 'GameState'
  info: 'UiInfo'
  def to_pb(self):
    obj = gui_pb2.StatusUpdate()
    obj.state.CopyFrom(self.state.to_pb())
    obj.info.CopyFrom(self.info.to_pb())
    return obj
  @staticmethod
  def from_pb(data) -> 'StatusUpdate':
    return StatusUpdate(state = GameState.from_pb(data.state),info = UiInfo.from_pb(data.info),)
  def serialize(self) -> bytes:
    return self.to_pb().SerializeToString()
  @classmethod
  def deserialize(cls, data: bytes) -> 'StatusUpdate':
    obj = gui_pb2.StatusUpdate()
    obj.ParseFromString(data);
    return cls.from_pb(obj)


@dataclass
class FrontendLocalStorage:
  api_key: 'str'
  def to_pb(self):
    obj = gui_pb2.FrontendLocalStorage()
    obj.api_key = self.api_key
    return obj
  @staticmethod
  def from_pb(data) -> 'FrontendLocalStorage':
    return FrontendLocalStorage(api_key = data.api_key,)
  def serialize(self) -> bytes:
    return self.to_pb().SerializeToString()
  @classmethod
  def deserialize(cls, data: bytes) -> 'FrontendLocalStorage':
    obj = gui_pb2.FrontendLocalStorage()
    obj.ParseFromString(data);
    return cls.from_pb(obj)


@dataclass
class SetThruster:
  thruster_name: 'str'
  amount: 'int'
  def to_pb(self):
    obj = gui_pb2.SetThruster()
    obj.thruster_name = self.thruster_name
    obj.amount = self.amount
    return obj
  @staticmethod
  def from_pb(data) -> 'SetThruster':
    return SetThruster(thruster_name = data.thruster_name,amount = data.amount,)
  def serialize(self) -> bytes:
    return self.to_pb().SerializeToString()
  @classmethod
  def deserialize(cls, data: bytes) -> 'SetThruster':
    obj = gui_pb2.SetThruster()
    obj.ParseFromString(data);
    return cls.from_pb(obj)


@dataclass
class IncreaseThruster:
  thruster_name: 'str'
  amount: 'int'
  def to_pb(self):
    obj = gui_pb2.IncreaseThruster()
    obj.thruster_name = self.thruster_name
    obj.amount = self.amount
    return obj
  @staticmethod
  def from_pb(data) -> 'IncreaseThruster':
    return IncreaseThruster(thruster_name = data.thruster_name,amount = data.amount,)
  def serialize(self) -> bytes:
    return self.to_pb().SerializeToString()
  @classmethod
  def deserialize(cls, data: bytes) -> 'IncreaseThruster':
    obj = gui_pb2.IncreaseThruster()
    obj.ParseFromString(data);
    return cls.from_pb(obj)


@dataclass
class MissionId:
  value: 'str'
  def to_pb(self):
    obj = gui_pb2.MissionId()
    obj.value = self.value
    return obj
  @staticmethod
  def from_pb(data) -> 'MissionId':
    return MissionId(value = data.value,)
  def serialize(self) -> bytes:
    return self.to_pb().SerializeToString()
  @classmethod
  def deserialize(cls, data: bytes) -> 'MissionId':
    obj = gui_pb2.MissionId()
    obj.ParseFromString(data);
    return cls.from_pb(obj)


@dataclass
class ResourceBuySell:
  what: 'ResourceType'
  buy_price: 'int'
  sell_price: 'int'
  def to_pb(self):
    obj = gui_pb2.ResourceBuySell()
    obj.what = self.what.to_pb()
    obj.buy_price = self.buy_price
    obj.sell_price = self.sell_price
    return obj
  @staticmethod
  def from_pb(data) -> 'ResourceBuySell':
    return ResourceBuySell(what = ResourceType.from_pb(data.what),buy_price = data.buy_price,sell_price = data.sell_price,)
  def serialize(self) -> bytes:
    return self.to_pb().SerializeToString()
  @classmethod
  def deserialize(cls, data: bytes) -> 'ResourceBuySell':
    obj = gui_pb2.ResourceBuySell()
    obj.ParseFromString(data);
    return cls.from_pb(obj)


@dataclass
class ItemId:
  value: 'str'
  def to_pb(self):
    obj = gui_pb2.ItemId()
    obj.value = self.value
    return obj
  @staticmethod
  def from_pb(data) -> 'ItemId':
    return ItemId(value = data.value,)
  def serialize(self) -> bytes:
    return self.to_pb().SerializeToString()
  @classmethod
  def deserialize(cls, data: bytes) -> 'ItemId':
    obj = gui_pb2.ItemId()
    obj.ParseFromString(data);
    return cls.from_pb(obj)


@dataclass
class ItemToBuy:
  item: 'ItemId'
  title: 'str'
  text: 'str'
  price: 'int'
  def to_pb(self):
    obj = gui_pb2.ItemToBuy()
    obj.item.CopyFrom(self.item.to_pb())
    obj.title = self.title
    obj.text = self.text
    obj.price = self.price
    return obj
  @staticmethod
  def from_pb(data) -> 'ItemToBuy':
    return ItemToBuy(item = ItemId.from_pb(data.item),title = data.title,text = data.text,price = data.price,)
  def serialize(self) -> bytes:
    return self.to_pb().SerializeToString()
  @classmethod
  def deserialize(cls, data: bytes) -> 'ItemToBuy':
    obj = gui_pb2.ItemToBuy()
    obj.ParseFromString(data);
    return cls.from_pb(obj)


@dataclass
class Market:
  resources: List['ResourceBuySell']
  items: List['ItemToBuy']
  def to_pb(self):
    obj = gui_pb2.Market()
    obj.resources.extend([element.to_pb() for element in self.resources])
    obj.items.extend([element.to_pb() for element in self.items])
    return obj
  @staticmethod
  def from_pb(data) -> 'Market':
    return Market(resources = [ResourceBuySell.from_pb(element) for element in data.resources],items = [ItemToBuy.from_pb(element) for element in data.items],)
  def serialize(self) -> bytes:
    return self.to_pb().SerializeToString()
  @classmethod
  def deserialize(cls, data: bytes) -> 'Market':
    obj = gui_pb2.Market()
    obj.ParseFromString(data);
    return cls.from_pb(obj)


@dataclass
class Mission:
  id: 'MissionId'
  title: 'str'
  text: 'str'
  def to_pb(self):
    obj = gui_pb2.Mission()
    obj.id.CopyFrom(self.id.to_pb())
    obj.title = self.title
    obj.text = self.text
    return obj
  @staticmethod
  def from_pb(data) -> 'Mission':
    return Mission(id = MissionId.from_pb(data.id),title = data.title,text = data.text,)
  def serialize(self) -> bytes:
    return self.to_pb().SerializeToString()
  @classmethod
  def deserialize(cls, data: bytes) -> 'Mission':
    obj = gui_pb2.Mission()
    obj.ParseFromString(data);
    return cls.from_pb(obj)


@dataclass
class Communication_SpaceStation:
  name: 'str'
  market: 'Market'
  missions: List['Mission']
  def to_pb(self):
    obj = gui_pb2.Communication_SpaceStation()
    obj.name = self.name
    obj.market.CopyFrom(self.market.to_pb())
    obj.missions.extend([element.to_pb() for element in self.missions])
    return obj
  @staticmethod
  def from_pb(data) -> 'Communication_SpaceStation':
    return Communication_SpaceStation(name = data.name,market = Market.from_pb(data.market),missions = [Mission.from_pb(element) for element in data.missions],)
  def serialize(self) -> bytes:
    return self.to_pb().SerializeToString()
  @classmethod
  def deserialize(cls, data: bytes) -> 'Communication_SpaceStation':
    obj = gui_pb2.Communication_SpaceStation()
    obj.ParseFromString(data);
    return cls.from_pb(obj)


@dataclass
class Communication:
  stations_in_range: List['Communication_SpaceStation']
  def to_pb(self):
    obj = gui_pb2.Communication()
    obj.stations_in_range.extend([element.to_pb() for element in self.stations_in_range])
    return obj
  @staticmethod
  def from_pb(data) -> 'Communication':
    return Communication(stations_in_range = [Communication_SpaceStation.from_pb(element) for element in data.stations_in_range],)
  def serialize(self) -> bytes:
    return self.to_pb().SerializeToString()
  @classmethod
  def deserialize(cls, data: bytes) -> 'Communication':
    obj = gui_pb2.Communication()
    obj.ParseFromString(data);
    return cls.from_pb(obj)


@dataclass
class ItemInHold:
  title: 'str'
  text: 'str'
  def to_pb(self):
    obj = gui_pb2.ItemInHold()
    obj.title = self.title
    obj.text = self.text
    return obj
  @staticmethod
  def from_pb(data) -> 'ItemInHold':
    return ItemInHold(title = data.title,text = data.text,)
  def serialize(self) -> bytes:
    return self.to_pb().SerializeToString()
  @classmethod
  def deserialize(cls, data: bytes) -> 'ItemInHold':
    obj = gui_pb2.ItemInHold()
    obj.ParseFromString(data);
    return cls.from_pb(obj)


@dataclass
class CargoHold:
  credits: 'int'
  hold_size: 'int'
  free_space: 'int'
  resources: List['ResourceInHold']
  items: List['ItemInHold']
  def to_pb(self):
    obj = gui_pb2.CargoHold()
    obj.credits = self.credits
    obj.hold_size = self.hold_size
    obj.free_space = self.free_space
    obj.resources.extend([element.to_pb() for element in self.resources])
    obj.items.extend([element.to_pb() for element in self.items])
    return obj
  @staticmethod
  def from_pb(data) -> 'CargoHold':
    return CargoHold(credits = data.credits,hold_size = data.hold_size,free_space = data.free_space,resources = [ResourceInHold.from_pb(element) for element in data.resources],items = [ItemInHold.from_pb(element) for element in data.items],)
  def serialize(self) -> bytes:
    return self.to_pb().SerializeToString()
  @classmethod
  def deserialize(cls, data: bytes) -> 'CargoHold':
    obj = gui_pb2.CargoHold()
    obj.ParseFromString(data);
    return cls.from_pb(obj)


@dataclass
class BuySell:
  station: 'str'
  what: 'ResourceType'
  def to_pb(self):
    obj = gui_pb2.BuySell()
    obj.station = self.station
    obj.what = self.what.to_pb()
    return obj
  @staticmethod
  def from_pb(data) -> 'BuySell':
    return BuySell(station = data.station,what = ResourceType.from_pb(data.what),)
  def serialize(self) -> bytes:
    return self.to_pb().SerializeToString()
  @classmethod
  def deserialize(cls, data: bytes) -> 'BuySell':
    obj = gui_pb2.BuySell()
    obj.ParseFromString(data);
    return cls.from_pb(obj)


@dataclass
class BuyItem:
  station: 'str'
  what: 'ItemId'
  def to_pb(self):
    obj = gui_pb2.BuyItem()
    obj.station = self.station
    obj.what.CopyFrom(self.what.to_pb())
    return obj
  @staticmethod
  def from_pb(data) -> 'BuyItem':
    return BuyItem(station = data.station,what = ItemId.from_pb(data.what),)
  def serialize(self) -> bytes:
    return self.to_pb().SerializeToString()
  @classmethod
  def deserialize(cls, data: bytes) -> 'BuyItem':
    obj = gui_pb2.BuyItem()
    obj.ParseFromString(data);
    return cls.from_pb(obj)


@dataclass
class MissionText:
  text: 'str'
  def to_pb(self):
    obj = gui_pb2.MissionText()
    obj.text = self.text
    return obj
  @staticmethod
  def from_pb(data) -> 'MissionText':
    return MissionText(text = data.text,)
  def serialize(self) -> bytes:
    return self.to_pb().SerializeToString()
  @classmethod
  def deserialize(cls, data: bytes) -> 'MissionText':
    obj = gui_pb2.MissionText()
    obj.ParseFromString(data);
    return cls.from_pb(obj)


@dataclass
class AcceptMission:
  mission: 'MissionId'
  def to_pb(self):
    obj = gui_pb2.AcceptMission()
    obj.mission.CopyFrom(self.mission.to_pb())
    return obj
  @staticmethod
  def from_pb(data) -> 'AcceptMission':
    return AcceptMission(mission = MissionId.from_pb(data.mission),)
  def serialize(self) -> bytes:
    return self.to_pb().SerializeToString()
  @classmethod
  def deserialize(cls, data: bytes) -> 'AcceptMission':
    obj = gui_pb2.AcceptMission()
    obj.ParseFromString(data);
    return cls.from_pb(obj)


@dataclass
class GetMissions_ActiveMission:
  title: 'str'
  texts: List['MissionText']
  def to_pb(self):
    obj = gui_pb2.GetMissions.ActiveMission()
    obj.title = self.title
    obj.texts.extend([element.to_pb() for element in self.texts])
    return obj
  @staticmethod
  def from_pb(data) -> 'GetMissions_ActiveMission':
    return GetMissions_ActiveMission(title = data.title,texts = [MissionText.from_pb(element) for element in data.texts],)
  def serialize(self) -> bytes:
    return self.to_pb().SerializeToString()
  @classmethod
  def deserialize(cls, data: bytes) -> 'GetMissions_ActiveMission':
    obj = gui_pb2.ActiveMission()
    obj.ParseFromString(data);
    return cls.from_pb(obj)


@dataclass
class GetMissions_FinishedMission:
  title: 'str'
  text: 'str'
  def to_pb(self):
    obj = gui_pb2.GetMissions.FinishedMission()
    obj.title = self.title
    obj.text = self.text
    return obj
  @staticmethod
  def from_pb(data) -> 'GetMissions_FinishedMission':
    return GetMissions_FinishedMission(title = data.title,text = data.text,)
  def serialize(self) -> bytes:
    return self.to_pb().SerializeToString()
  @classmethod
  def deserialize(cls, data: bytes) -> 'GetMissions_FinishedMission':
    obj = gui_pb2.FinishedMission()
    obj.ParseFromString(data);
    return cls.from_pb(obj)


@dataclass
class GetMissions:
  active_missions: List['GetMissions_ActiveMission']
  finished_missions: List['GetMissions_FinishedMission']
  def to_pb(self):
    obj = gui_pb2.GetMissions()
    obj.active_missions.extend([element.to_pb() for element in self.active_missions])
    obj.finished_missions.extend([element.to_pb() for element in self.finished_missions])
    return obj
  @staticmethod
  def from_pb(data) -> 'GetMissions':
    return GetMissions(active_missions = [GetMissions_ActiveMission.from_pb(element) for element in data.active_missions],finished_missions = [GetMissions_FinishedMission.from_pb(element) for element in data.finished_missions],)
  def serialize(self) -> bytes:
    return self.to_pb().SerializeToString()
  @classmethod
  def deserialize(cls, data: bytes) -> 'GetMissions':
    obj = gui_pb2.GetMissions()
    obj.ParseFromString(data);
    return cls.from_pb(obj)


@dataclass
class InstallableSoftware:
  name: 'str'
  def to_pb(self):
    obj = gui_pb2.InstallableSoftware()
    obj.name = self.name
    return obj
  @staticmethod
  def from_pb(data) -> 'InstallableSoftware':
    return InstallableSoftware(name = data.name,)
  def serialize(self) -> bytes:
    return self.to_pb().SerializeToString()
  @classmethod
  def deserialize(cls, data: bytes) -> 'InstallableSoftware':
    obj = gui_pb2.InstallableSoftware()
    obj.ParseFromString(data);
    return cls.from_pb(obj)


@dataclass
class GetInstallableSoftware:
  list: List['InstallableSoftware']
  k8s_state: 'K8sState'
  def to_pb(self):
    obj = gui_pb2.GetInstallableSoftware()
    obj.list.extend([element.to_pb() for element in self.list])
    obj.k8s_state.CopyFrom(self.k8s_state.to_pb())
    return obj
  @staticmethod
  def from_pb(data) -> 'GetInstallableSoftware':
    return GetInstallableSoftware(list = [InstallableSoftware.from_pb(element) for element in data.list],k8s_state = K8sState.from_pb(data.k8s_state),)
  def serialize(self) -> bytes:
    return self.to_pb().SerializeToString()
  @classmethod
  def deserialize(cls, data: bytes) -> 'GetInstallableSoftware':
    obj = gui_pb2.GetInstallableSoftware()
    obj.ParseFromString(data);
    return cls.from_pb(obj)


@dataclass
class DeployInstallableSoftware:
  name: 'str'
  def to_pb(self):
    obj = gui_pb2.DeployInstallableSoftware()
    obj.name = self.name
    return obj
  @staticmethod
  def from_pb(data) -> 'DeployInstallableSoftware':
    return DeployInstallableSoftware(name = data.name,)
  def serialize(self) -> bytes:
    return self.to_pb().SerializeToString()
  @classmethod
  def deserialize(cls, data: bytes) -> 'DeployInstallableSoftware':
    obj = gui_pb2.DeployInstallableSoftware()
    obj.ParseFromString(data);
    return cls.from_pb(obj)


@dataclass
class UndeployInstallableSoftware:
  name: 'str'
  def to_pb(self):
    obj = gui_pb2.UndeployInstallableSoftware()
    obj.name = self.name
    return obj
  @staticmethod
  def from_pb(data) -> 'UndeployInstallableSoftware':
    return UndeployInstallableSoftware(name = data.name,)
  def serialize(self) -> bytes:
    return self.to_pb().SerializeToString()
  @classmethod
  def deserialize(cls, data: bytes) -> 'UndeployInstallableSoftware':
    obj = gui_pb2.UndeployInstallableSoftware()
    obj.ParseFromString(data);
    return cls.from_pb(obj)


@dataclass
class WipeInstallableSoftware:
  def to_pb(self):
    obj = gui_pb2.WipeInstallableSoftware()
    return obj
  @staticmethod
  def from_pb(data) -> 'WipeInstallableSoftware':
    return WipeInstallableSoftware()
  def serialize(self) -> bytes:
    return self.to_pb().SerializeToString()
  @classmethod
  def deserialize(cls, data: bytes) -> 'WipeInstallableSoftware':
    obj = gui_pb2.WipeInstallableSoftware()
    obj.ParseFromString(data);
    return cls.from_pb(obj)


@dataclass
class NearObject:
  name: 'str'
  pos: 'Vec'
  def to_pb(self):
    obj = gui_pb2.NearObject()
    obj.name = self.name
    obj.pos.CopyFrom(self.pos.to_pb())
    return obj
  @staticmethod
  def from_pb(data) -> 'NearObject':
    return NearObject(name = data.name,pos = Vec.from_pb(data.pos),)
  def serialize(self) -> bytes:
    return self.to_pb().SerializeToString()
  @classmethod
  def deserialize(cls, data: bytes) -> 'NearObject':
    obj = gui_pb2.NearObject()
    obj.ParseFromString(data);
    return cls.from_pb(obj)


@dataclass
class ScannerGetNearObjects:
  near_objects: List['NearObject']
  def to_pb(self):
    obj = gui_pb2.ScannerGetNearObjects()
    obj.near_objects.extend([element.to_pb() for element in self.near_objects])
    return obj
  @staticmethod
  def from_pb(data) -> 'ScannerGetNearObjects':
    return ScannerGetNearObjects(near_objects = [NearObject.from_pb(element) for element in data.near_objects],)
  def serialize(self) -> bytes:
    return self.to_pb().SerializeToString()
  @classmethod
  def deserialize(cls, data: bytes) -> 'ScannerGetNearObjects':
    obj = gui_pb2.ScannerGetNearObjects()
    obj.ParseFromString(data);
    return cls.from_pb(obj)


@dataclass
class LaserGetState:
  active: 'bool'
  is_mining: 'bool'
  is_cooling_down: 'bool'
  def to_pb(self):
    obj = gui_pb2.LaserGetState()
    obj.active = self.active
    obj.is_mining = self.is_mining
    obj.is_cooling_down = self.is_cooling_down
    return obj
  @staticmethod
  def from_pb(data) -> 'LaserGetState':
    return LaserGetState(active = data.active,is_mining = data.is_mining,is_cooling_down = data.is_cooling_down,)
  def serialize(self) -> bytes:
    return self.to_pb().SerializeToString()
  @classmethod
  def deserialize(cls, data: bytes) -> 'LaserGetState':
    obj = gui_pb2.LaserGetState()
    obj.ParseFromString(data);
    return cls.from_pb(obj)


@dataclass
class LaserSetState:
  active: 'bool'
  def to_pb(self):
    obj = gui_pb2.LaserSetState()
    obj.active = self.active
    return obj
  @staticmethod
  def from_pb(data) -> 'LaserSetState':
    return LaserSetState(active = data.active,)
  def serialize(self) -> bytes:
    return self.to_pb().SerializeToString()
  @classmethod
  def deserialize(cls, data: bytes) -> 'LaserSetState':
    obj = gui_pb2.LaserSetState()
    obj.ParseFromString(data);
    return cls.from_pb(obj)


@dataclass
class LaserSetRotationState:
  angle: 'float'
  def to_pb(self):
    obj = gui_pb2.LaserSetRotationState()
    obj.angle = self.angle
    return obj
  @staticmethod
  def from_pb(data) -> 'LaserSetRotationState':
    return LaserSetRotationState(angle = float("{:.7f}".format(data.angle)),)
  def serialize(self) -> bytes:
    return self.to_pb().SerializeToString()
  @classmethod
  def deserialize(cls, data: bytes) -> 'LaserSetRotationState':
    obj = gui_pb2.LaserSetRotationState()
    obj.ParseFromString(data);
    return cls.from_pb(obj)


@dataclass
class CommSend:
  source: 'str'
  target: 'str'
  data: 'bytes'
  def to_pb(self):
    obj = gui_pb2.CommSend()
    obj.source = self.source
    obj.target = self.target
    obj.data = self.data
    return obj
  @staticmethod
  def from_pb(data) -> 'CommSend':
    return CommSend(source = data.source,target = data.target,data = data.data,)
  def serialize(self) -> bytes:
    return self.to_pb().SerializeToString()
  @classmethod
  def deserialize(cls, data: bytes) -> 'CommSend':
    obj = gui_pb2.CommSend()
    obj.ParseFromString(data);
    return cls.from_pb(obj)


@dataclass
class CommReceiveRequest:
  source: 'str'
  def to_pb(self):
    obj = gui_pb2.CommReceiveRequest()
    obj.source = self.source
    return obj
  @staticmethod
  def from_pb(data) -> 'CommReceiveRequest':
    return CommReceiveRequest(source = data.source,)
  def serialize(self) -> bytes:
    return self.to_pb().SerializeToString()
  @classmethod
  def deserialize(cls, data: bytes) -> 'CommReceiveRequest':
    obj = gui_pb2.CommReceiveRequest()
    obj.ParseFromString(data);
    return cls.from_pb(obj)


@dataclass
class CommReceiveResponse_Message:
  target: 'str'
  data: 'bytes'
  def to_pb(self):
    obj = gui_pb2.CommReceiveResponse.Message()
    obj.target = self.target
    obj.data = self.data
    return obj
  @staticmethod
  def from_pb(data) -> 'CommReceiveResponse_Message':
    return CommReceiveResponse_Message(target = data.target,data = data.data,)
  def serialize(self) -> bytes:
    return self.to_pb().SerializeToString()
  @classmethod
  def deserialize(cls, data: bytes) -> 'CommReceiveResponse_Message':
    obj = gui_pb2.Message()
    obj.ParseFromString(data);
    return cls.from_pb(obj)


@dataclass
class CommReceiveResponse:
  messages: List['CommReceiveResponse_Message']
  def to_pb(self):
    obj = gui_pb2.CommReceiveResponse()
    obj.messages.extend([element.to_pb() for element in self.messages])
    return obj
  @staticmethod
  def from_pb(data) -> 'CommReceiveResponse':
    return CommReceiveResponse(messages = [CommReceiveResponse_Message.from_pb(element) for element in data.messages],)
  def serialize(self) -> bytes:
    return self.to_pb().SerializeToString()
  @classmethod
  def deserialize(cls, data: bytes) -> 'CommReceiveResponse':
    obj = gui_pb2.CommReceiveResponse()
    obj.ParseFromString(data);
    return cls.from_pb(obj)


class TheShipGuiClient():
  def __init__(self, channel: gui_grpc.grpclib.client.Channel, metadata: Dict[str, str] = {}):
    self.client = gui_grpc.TheShipGuiStub(channel)
    self.metadata = metadata
  
  async def getUpdates(self, request: Void, metadata: Dict[str, str] = {}) -> AsyncGenerator[StatusUpdate, None]:
    async with self.client.getUpdates.open(metadata = self.metadata | metadata) as stream:
      await stream.send_request()
      await stream.send_message(request.to_pb())
      while True:
        message = await stream.recv_message()
        if message is None:
          break
        yield StatusUpdate.from_pb(message)
  
  async def execute(self, request: Void, metadata: Dict[str, str] = {}) -> Void:
    async with self.client.execute.open(metadata = self.metadata | metadata) as stream:
      await stream.send_request()
      await stream.send_message(request.to_pb())
      message = await stream.recv_message()
      return Void.from_pb(message)
  
  async def stopShip(self, request: Void, metadata: Dict[str, str] = {}) -> Void:
    async with self.client.stopShip.open(metadata = self.metadata | metadata) as stream:
      await stream.send_request()
      await stream.send_message(request.to_pb())
      message = await stream.recv_message()
      return Void.from_pb(message)
  
  async def setState(self, request: GameState, metadata: Dict[str, str] = {}) -> Void:
    async with self.client.setState.open(metadata = self.metadata | metadata) as stream:
      await stream.send_request()
      await stream.send_message(request.to_pb())
      message = await stream.recv_message()
      return Void.from_pb(message)
  


class TheShipGuiInterface(abc.ABC):
  async def getUpdates(self, request: Void, metadata: Dict[str, str]) -> AsyncGenerator[StatusUpdate, None]:
    pass
  
  async def execute(self, request: Void, metadata: Dict[str, str]) -> Void:
    pass
  
  async def stopShip(self, request: Void, metadata: Dict[str, str]) -> Void:
    pass
  
  async def setState(self, request: GameState, metadata: Dict[str, str]) -> Void:
    pass
  
  class TheShipGuiForwarder(gui_grpc.TheShipGuiBase):
    def __init__(self, impl: 'TheShipGuiInterface'):
      self.impl = impl
    
    async def getUpdates(self, stream) -> None:
      assert stream.metadata is not None
      metadata = {entry[0]: entry[1] for entry in stream.metadata.items() if isinstance(entry[1], str)}
      raw_request = await stream.recv_message()
      assert raw_request is not None
      request = Void.from_pb(raw_request)
      try:
        response = self.impl.getUpdates(request = request, metadata = metadata)
        async for message in response:
          await stream.send_message(message.to_pb())
      except KeyboardInterrupt:
        raise
      except GrpcAccessDenied as ex:
        await stream.send_trailing_metadata(status=gui_grpc.grpclib.Status.PERMISSION_DENIED)
      except Exception as ex:
        print(ex)
        await stream.send_trailing_metadata(status=gui_grpc.grpclib.Status.UNKNOWN)
    
    async def execute(self, stream) -> None:
      assert stream.metadata is not None
      metadata = {entry[0]: entry[1] for entry in stream.metadata.items() if isinstance(entry[1], str)}
      raw_request = await stream.recv_message()
      assert raw_request is not None
      request = Void.from_pb(raw_request)
      try:
        response = await self.impl.execute(request = request, metadata = metadata)
        await stream.send_message(response.to_pb())
      except KeyboardInterrupt:
        raise
      except GrpcAccessDenied as ex:
        await stream.send_trailing_metadata(status=gui_grpc.grpclib.Status.PERMISSION_DENIED)
      except Exception as ex:
        print(ex)
        await stream.send_trailing_metadata(status=gui_grpc.grpclib.Status.UNKNOWN)
    
    async def stopShip(self, stream) -> None:
      assert stream.metadata is not None
      metadata = {entry[0]: entry[1] for entry in stream.metadata.items() if isinstance(entry[1], str)}
      raw_request = await stream.recv_message()
      assert raw_request is not None
      request = Void.from_pb(raw_request)
      try:
        response = await self.impl.stopShip(request = request, metadata = metadata)
        await stream.send_message(response.to_pb())
      except KeyboardInterrupt:
        raise
      except GrpcAccessDenied as ex:
        await stream.send_trailing_metadata(status=gui_grpc.grpclib.Status.PERMISSION_DENIED)
      except Exception as ex:
        print(ex)
        await stream.send_trailing_metadata(status=gui_grpc.grpclib.Status.UNKNOWN)
    
    async def setState(self, stream) -> None:
      assert stream.metadata is not None
      metadata = {entry[0]: entry[1] for entry in stream.metadata.items() if isinstance(entry[1], str)}
      raw_request = await stream.recv_message()
      assert raw_request is not None
      request = GameState.from_pb(raw_request)
      try:
        response = await self.impl.setState(request = request, metadata = metadata)
        await stream.send_message(response.to_pb())
      except KeyboardInterrupt:
        raise
      except GrpcAccessDenied as ex:
        await stream.send_trailing_metadata(status=gui_grpc.grpclib.Status.PERMISSION_DENIED)
      except Exception as ex:
        print(ex)
        await stream.send_trailing_metadata(status=gui_grpc.grpclib.Status.UNKNOWN)
    
  
  def make_handler(self):
    return TheShipGuiInterface.TheShipGuiForwarder(self)
  

class TheShipLowLevelClient():
  def __init__(self, channel: gui_grpc.grpclib.client.Channel, metadata: Dict[str, str] = {}):
    self.client = gui_grpc.TheShipLowLevelStub(channel)
    self.metadata = metadata
  
  async def getPos(self, request: Void, metadata: Dict[str, str] = {}) -> PhysicalObject:
    async with self.client.getPos.open(metadata = self.metadata | metadata) as stream:
      await stream.send_request()
      await stream.send_message(request.to_pb())
      message = await stream.recv_message()
      return PhysicalObject.from_pb(message)
  
  async def setThruster(self, request: SetThruster, metadata: Dict[str, str] = {}) -> Void:
    async with self.client.setThruster.open(metadata = self.metadata | metadata) as stream:
      await stream.send_request()
      await stream.send_message(request.to_pb())
      message = await stream.recv_message()
      return Void.from_pb(message)
  
  async def increaseThruster(self, request: IncreaseThruster, metadata: Dict[str, str] = {}) -> Void:
    async with self.client.increaseThruster.open(metadata = self.metadata | metadata) as stream:
      await stream.send_request()
      await stream.send_message(request.to_pb())
      message = await stream.recv_message()
      return Void.from_pb(message)
  
  async def getCommunication(self, request: Void, metadata: Dict[str, str] = {}) -> Communication:
    async with self.client.getCommunication.open(metadata = self.metadata | metadata) as stream:
      await stream.send_request()
      await stream.send_message(request.to_pb())
      message = await stream.recv_message()
      return Communication.from_pb(message)
  
  async def sellResource(self, request: BuySell, metadata: Dict[str, str] = {}) -> Void:
    async with self.client.sellResource.open(metadata = self.metadata | metadata) as stream:
      await stream.send_request()
      await stream.send_message(request.to_pb())
      message = await stream.recv_message()
      return Void.from_pb(message)
  
  async def buyResource(self, request: BuySell, metadata: Dict[str, str] = {}) -> Void:
    async with self.client.buyResource.open(metadata = self.metadata | metadata) as stream:
      await stream.send_request()
      await stream.send_message(request.to_pb())
      message = await stream.recv_message()
      return Void.from_pb(message)
  
  async def buyItem(self, request: BuyItem, metadata: Dict[str, str] = {}) -> Void:
    async with self.client.buyItem.open(metadata = self.metadata | metadata) as stream:
      await stream.send_request()
      await stream.send_message(request.to_pb())
      message = await stream.recv_message()
      return Void.from_pb(message)
  
  async def acceptMission(self, request: AcceptMission, metadata: Dict[str, str] = {}) -> Void:
    async with self.client.acceptMission.open(metadata = self.metadata | metadata) as stream:
      await stream.send_request()
      await stream.send_message(request.to_pb())
      message = await stream.recv_message()
      return Void.from_pb(message)
  
  async def getCargoHold(self, request: Void, metadata: Dict[str, str] = {}) -> CargoHold:
    async with self.client.getCargoHold.open(metadata = self.metadata | metadata) as stream:
      await stream.send_request()
      await stream.send_message(request.to_pb())
      message = await stream.recv_message()
      return CargoHold.from_pb(message)
  
  async def getMissions(self, request: Void, metadata: Dict[str, str] = {}) -> GetMissions:
    async with self.client.getMissions.open(metadata = self.metadata | metadata) as stream:
      await stream.send_request()
      await stream.send_message(request.to_pb())
      message = await stream.recv_message()
      return GetMissions.from_pb(message)
  
  async def getInstallableSoftware(self, request: Void, metadata: Dict[str, str] = {}) -> GetInstallableSoftware:
    async with self.client.getInstallableSoftware.open(metadata = self.metadata | metadata) as stream:
      await stream.send_request()
      await stream.send_message(request.to_pb())
      message = await stream.recv_message()
      return GetInstallableSoftware.from_pb(message)
  
  async def deployInstallableSoftware(self, request: DeployInstallableSoftware, metadata: Dict[str, str] = {}) -> Void:
    async with self.client.deployInstallableSoftware.open(metadata = self.metadata | metadata) as stream:
      await stream.send_request()
      await stream.send_message(request.to_pb())
      message = await stream.recv_message()
      return Void.from_pb(message)
  
  async def undeployInstallableSoftware(self, request: UndeployInstallableSoftware, metadata: Dict[str, str] = {}) -> Void:
    async with self.client.undeployInstallableSoftware.open(metadata = self.metadata | metadata) as stream:
      await stream.send_request()
      await stream.send_message(request.to_pb())
      message = await stream.recv_message()
      return Void.from_pb(message)
  
  async def wipeInstallableSoftware(self, request: WipeInstallableSoftware, metadata: Dict[str, str] = {}) -> Void:
    async with self.client.wipeInstallableSoftware.open(metadata = self.metadata | metadata) as stream:
      await stream.send_request()
      await stream.send_message(request.to_pb())
      message = await stream.recv_message()
      return Void.from_pb(message)
  
  async def scannerGetNearObjects(self, request: Void, metadata: Dict[str, str] = {}) -> ScannerGetNearObjects:
    async with self.client.scannerGetNearObjects.open(metadata = self.metadata | metadata) as stream:
      await stream.send_request()
      await stream.send_message(request.to_pb())
      message = await stream.recv_message()
      return ScannerGetNearObjects.from_pb(message)
  
  async def laserGetState(self, request: Void, metadata: Dict[str, str] = {}) -> LaserGetState:
    async with self.client.laserGetState.open(metadata = self.metadata | metadata) as stream:
      await stream.send_request()
      await stream.send_message(request.to_pb())
      message = await stream.recv_message()
      return LaserGetState.from_pb(message)
  
  async def laserSetState(self, request: LaserSetState, metadata: Dict[str, str] = {}) -> Void:
    async with self.client.laserSetState.open(metadata = self.metadata | metadata) as stream:
      await stream.send_request()
      await stream.send_message(request.to_pb())
      message = await stream.recv_message()
      return Void.from_pb(message)
  
  async def laserSetRotationState(self, request: LaserSetRotationState, metadata: Dict[str, str] = {}) -> Void:
    async with self.client.laserSetRotationState.open(metadata = self.metadata | metadata) as stream:
      await stream.send_request()
      await stream.send_message(request.to_pb())
      message = await stream.recv_message()
      return Void.from_pb(message)
  
  async def commSend(self, request: CommSend, metadata: Dict[str, str] = {}) -> Void:
    async with self.client.commSend.open(metadata = self.metadata | metadata) as stream:
      await stream.send_request()
      await stream.send_message(request.to_pb())
      message = await stream.recv_message()
      return Void.from_pb(message)
  
  async def commReceive(self, request: CommReceiveRequest, metadata: Dict[str, str] = {}) -> CommReceiveResponse:
    async with self.client.commReceive.open(metadata = self.metadata | metadata) as stream:
      await stream.send_request()
      await stream.send_message(request.to_pb())
      message = await stream.recv_message()
      return CommReceiveResponse.from_pb(message)
  


class TheShipLowLevelInterface(abc.ABC):
  async def getPos(self, request: Void, metadata: Dict[str, str]) -> PhysicalObject:
    pass
  
  async def setThruster(self, request: SetThruster, metadata: Dict[str, str]) -> Void:
    pass
  
  async def increaseThruster(self, request: IncreaseThruster, metadata: Dict[str, str]) -> Void:
    pass
  
  async def getCommunication(self, request: Void, metadata: Dict[str, str]) -> Communication:
    pass
  
  async def sellResource(self, request: BuySell, metadata: Dict[str, str]) -> Void:
    pass
  
  async def buyResource(self, request: BuySell, metadata: Dict[str, str]) -> Void:
    pass
  
  async def buyItem(self, request: BuyItem, metadata: Dict[str, str]) -> Void:
    pass
  
  async def acceptMission(self, request: AcceptMission, metadata: Dict[str, str]) -> Void:
    pass
  
  async def getCargoHold(self, request: Void, metadata: Dict[str, str]) -> CargoHold:
    pass
  
  async def getMissions(self, request: Void, metadata: Dict[str, str]) -> GetMissions:
    pass
  
  async def getInstallableSoftware(self, request: Void, metadata: Dict[str, str]) -> GetInstallableSoftware:
    pass
  
  async def deployInstallableSoftware(self, request: DeployInstallableSoftware, metadata: Dict[str, str]) -> Void:
    pass
  
  async def undeployInstallableSoftware(self, request: UndeployInstallableSoftware, metadata: Dict[str, str]) -> Void:
    pass
  
  async def wipeInstallableSoftware(self, request: WipeInstallableSoftware, metadata: Dict[str, str]) -> Void:
    pass
  
  async def scannerGetNearObjects(self, request: Void, metadata: Dict[str, str]) -> ScannerGetNearObjects:
    pass
  
  async def laserGetState(self, request: Void, metadata: Dict[str, str]) -> LaserGetState:
    pass
  
  async def laserSetState(self, request: LaserSetState, metadata: Dict[str, str]) -> Void:
    pass
  
  async def laserSetRotationState(self, request: LaserSetRotationState, metadata: Dict[str, str]) -> Void:
    pass
  
  async def commSend(self, request: CommSend, metadata: Dict[str, str]) -> Void:
    pass
  
  async def commReceive(self, request: CommReceiveRequest, metadata: Dict[str, str]) -> CommReceiveResponse:
    pass
  
  class TheShipLowLevelForwarder(gui_grpc.TheShipLowLevelBase):
    def __init__(self, impl: 'TheShipLowLevelInterface'):
      self.impl = impl
    
    async def getPos(self, stream) -> None:
      assert stream.metadata is not None
      metadata = {entry[0]: entry[1] for entry in stream.metadata.items() if isinstance(entry[1], str)}
      raw_request = await stream.recv_message()
      assert raw_request is not None
      request = Void.from_pb(raw_request)
      try:
        response = await self.impl.getPos(request = request, metadata = metadata)
        await stream.send_message(response.to_pb())
      except KeyboardInterrupt:
        raise
      except GrpcAccessDenied as ex:
        await stream.send_trailing_metadata(status=gui_grpc.grpclib.Status.PERMISSION_DENIED)
      except Exception as ex:
        print(ex)
        await stream.send_trailing_metadata(status=gui_grpc.grpclib.Status.UNKNOWN)
    
    async def setThruster(self, stream) -> None:
      assert stream.metadata is not None
      metadata = {entry[0]: entry[1] for entry in stream.metadata.items() if isinstance(entry[1], str)}
      raw_request = await stream.recv_message()
      assert raw_request is not None
      request = SetThruster.from_pb(raw_request)
      try:
        response = await self.impl.setThruster(request = request, metadata = metadata)
        await stream.send_message(response.to_pb())
      except KeyboardInterrupt:
        raise
      except GrpcAccessDenied as ex:
        await stream.send_trailing_metadata(status=gui_grpc.grpclib.Status.PERMISSION_DENIED)
      except Exception as ex:
        print(ex)
        await stream.send_trailing_metadata(status=gui_grpc.grpclib.Status.UNKNOWN)
    
    async def increaseThruster(self, stream) -> None:
      assert stream.metadata is not None
      metadata = {entry[0]: entry[1] for entry in stream.metadata.items() if isinstance(entry[1], str)}
      raw_request = await stream.recv_message()
      assert raw_request is not None
      request = IncreaseThruster.from_pb(raw_request)
      try:
        response = await self.impl.increaseThruster(request = request, metadata = metadata)
        await stream.send_message(response.to_pb())
      except KeyboardInterrupt:
        raise
      except GrpcAccessDenied as ex:
        await stream.send_trailing_metadata(status=gui_grpc.grpclib.Status.PERMISSION_DENIED)
      except Exception as ex:
        print(ex)
        await stream.send_trailing_metadata(status=gui_grpc.grpclib.Status.UNKNOWN)
    
    async def getCommunication(self, stream) -> None:
      assert stream.metadata is not None
      metadata = {entry[0]: entry[1] for entry in stream.metadata.items() if isinstance(entry[1], str)}
      raw_request = await stream.recv_message()
      assert raw_request is not None
      request = Void.from_pb(raw_request)
      try:
        response = await self.impl.getCommunication(request = request, metadata = metadata)
        await stream.send_message(response.to_pb())
      except KeyboardInterrupt:
        raise
      except GrpcAccessDenied as ex:
        await stream.send_trailing_metadata(status=gui_grpc.grpclib.Status.PERMISSION_DENIED)
      except Exception as ex:
        print(ex)
        await stream.send_trailing_metadata(status=gui_grpc.grpclib.Status.UNKNOWN)
    
    async def sellResource(self, stream) -> None:
      assert stream.metadata is not None
      metadata = {entry[0]: entry[1] for entry in stream.metadata.items() if isinstance(entry[1], str)}
      raw_request = await stream.recv_message()
      assert raw_request is not None
      request = BuySell.from_pb(raw_request)
      try:
        response = await self.impl.sellResource(request = request, metadata = metadata)
        await stream.send_message(response.to_pb())
      except KeyboardInterrupt:
        raise
      except GrpcAccessDenied as ex:
        await stream.send_trailing_metadata(status=gui_grpc.grpclib.Status.PERMISSION_DENIED)
      except Exception as ex:
        print(ex)
        await stream.send_trailing_metadata(status=gui_grpc.grpclib.Status.UNKNOWN)
    
    async def buyResource(self, stream) -> None:
      assert stream.metadata is not None
      metadata = {entry[0]: entry[1] for entry in stream.metadata.items() if isinstance(entry[1], str)}
      raw_request = await stream.recv_message()
      assert raw_request is not None
      request = BuySell.from_pb(raw_request)
      try:
        response = await self.impl.buyResource(request = request, metadata = metadata)
        await stream.send_message(response.to_pb())
      except KeyboardInterrupt:
        raise
      except GrpcAccessDenied as ex:
        await stream.send_trailing_metadata(status=gui_grpc.grpclib.Status.PERMISSION_DENIED)
      except Exception as ex:
        print(ex)
        await stream.send_trailing_metadata(status=gui_grpc.grpclib.Status.UNKNOWN)
    
    async def buyItem(self, stream) -> None:
      assert stream.metadata is not None
      metadata = {entry[0]: entry[1] for entry in stream.metadata.items() if isinstance(entry[1], str)}
      raw_request = await stream.recv_message()
      assert raw_request is not None
      request = BuyItem.from_pb(raw_request)
      try:
        response = await self.impl.buyItem(request = request, metadata = metadata)
        await stream.send_message(response.to_pb())
      except KeyboardInterrupt:
        raise
      except GrpcAccessDenied as ex:
        await stream.send_trailing_metadata(status=gui_grpc.grpclib.Status.PERMISSION_DENIED)
      except Exception as ex:
        print(ex)
        await stream.send_trailing_metadata(status=gui_grpc.grpclib.Status.UNKNOWN)
    
    async def acceptMission(self, stream) -> None:
      assert stream.metadata is not None
      metadata = {entry[0]: entry[1] for entry in stream.metadata.items() if isinstance(entry[1], str)}
      raw_request = await stream.recv_message()
      assert raw_request is not None
      request = AcceptMission.from_pb(raw_request)
      try:
        response = await self.impl.acceptMission(request = request, metadata = metadata)
        await stream.send_message(response.to_pb())
      except KeyboardInterrupt:
        raise
      except GrpcAccessDenied as ex:
        await stream.send_trailing_metadata(status=gui_grpc.grpclib.Status.PERMISSION_DENIED)
      except Exception as ex:
        print(ex)
        await stream.send_trailing_metadata(status=gui_grpc.grpclib.Status.UNKNOWN)
    
    async def getCargoHold(self, stream) -> None:
      assert stream.metadata is not None
      metadata = {entry[0]: entry[1] for entry in stream.metadata.items() if isinstance(entry[1], str)}
      raw_request = await stream.recv_message()
      assert raw_request is not None
      request = Void.from_pb(raw_request)
      try:
        response = await self.impl.getCargoHold(request = request, metadata = metadata)
        await stream.send_message(response.to_pb())
      except KeyboardInterrupt:
        raise
      except GrpcAccessDenied as ex:
        await stream.send_trailing_metadata(status=gui_grpc.grpclib.Status.PERMISSION_DENIED)
      except Exception as ex:
        print(ex)
        await stream.send_trailing_metadata(status=gui_grpc.grpclib.Status.UNKNOWN)
    
    async def getMissions(self, stream) -> None:
      assert stream.metadata is not None
      metadata = {entry[0]: entry[1] for entry in stream.metadata.items() if isinstance(entry[1], str)}
      raw_request = await stream.recv_message()
      assert raw_request is not None
      request = Void.from_pb(raw_request)
      try:
        response = await self.impl.getMissions(request = request, metadata = metadata)
        await stream.send_message(response.to_pb())
      except KeyboardInterrupt:
        raise
      except GrpcAccessDenied as ex:
        await stream.send_trailing_metadata(status=gui_grpc.grpclib.Status.PERMISSION_DENIED)
      except Exception as ex:
        print(ex)
        await stream.send_trailing_metadata(status=gui_grpc.grpclib.Status.UNKNOWN)
    
    async def getInstallableSoftware(self, stream) -> None:
      assert stream.metadata is not None
      metadata = {entry[0]: entry[1] for entry in stream.metadata.items() if isinstance(entry[1], str)}
      raw_request = await stream.recv_message()
      assert raw_request is not None
      request = Void.from_pb(raw_request)
      try:
        response = await self.impl.getInstallableSoftware(request = request, metadata = metadata)
        await stream.send_message(response.to_pb())
      except KeyboardInterrupt:
        raise
      except GrpcAccessDenied as ex:
        await stream.send_trailing_metadata(status=gui_grpc.grpclib.Status.PERMISSION_DENIED)
      except Exception as ex:
        print(ex)
        await stream.send_trailing_metadata(status=gui_grpc.grpclib.Status.UNKNOWN)
    
    async def deployInstallableSoftware(self, stream) -> None:
      assert stream.metadata is not None
      metadata = {entry[0]: entry[1] for entry in stream.metadata.items() if isinstance(entry[1], str)}
      raw_request = await stream.recv_message()
      assert raw_request is not None
      request = DeployInstallableSoftware.from_pb(raw_request)
      try:
        response = await self.impl.deployInstallableSoftware(request = request, metadata = metadata)
        await stream.send_message(response.to_pb())
      except KeyboardInterrupt:
        raise
      except GrpcAccessDenied as ex:
        await stream.send_trailing_metadata(status=gui_grpc.grpclib.Status.PERMISSION_DENIED)
      except Exception as ex:
        print(ex)
        await stream.send_trailing_metadata(status=gui_grpc.grpclib.Status.UNKNOWN)
    
    async def undeployInstallableSoftware(self, stream) -> None:
      assert stream.metadata is not None
      metadata = {entry[0]: entry[1] for entry in stream.metadata.items() if isinstance(entry[1], str)}
      raw_request = await stream.recv_message()
      assert raw_request is not None
      request = UndeployInstallableSoftware.from_pb(raw_request)
      try:
        response = await self.impl.undeployInstallableSoftware(request = request, metadata = metadata)
        await stream.send_message(response.to_pb())
      except KeyboardInterrupt:
        raise
      except GrpcAccessDenied as ex:
        await stream.send_trailing_metadata(status=gui_grpc.grpclib.Status.PERMISSION_DENIED)
      except Exception as ex:
        print(ex)
        await stream.send_trailing_metadata(status=gui_grpc.grpclib.Status.UNKNOWN)
    
    async def wipeInstallableSoftware(self, stream) -> None:
      assert stream.metadata is not None
      metadata = {entry[0]: entry[1] for entry in stream.metadata.items() if isinstance(entry[1], str)}
      raw_request = await stream.recv_message()
      assert raw_request is not None
      request = WipeInstallableSoftware.from_pb(raw_request)
      try:
        response = await self.impl.wipeInstallableSoftware(request = request, metadata = metadata)
        await stream.send_message(response.to_pb())
      except KeyboardInterrupt:
        raise
      except GrpcAccessDenied as ex:
        await stream.send_trailing_metadata(status=gui_grpc.grpclib.Status.PERMISSION_DENIED)
      except Exception as ex:
        print(ex)
        await stream.send_trailing_metadata(status=gui_grpc.grpclib.Status.UNKNOWN)
    
    async def scannerGetNearObjects(self, stream) -> None:
      assert stream.metadata is not None
      metadata = {entry[0]: entry[1] for entry in stream.metadata.items() if isinstance(entry[1], str)}
      raw_request = await stream.recv_message()
      assert raw_request is not None
      request = Void.from_pb(raw_request)
      try:
        response = await self.impl.scannerGetNearObjects(request = request, metadata = metadata)
        await stream.send_message(response.to_pb())
      except KeyboardInterrupt:
        raise
      except GrpcAccessDenied as ex:
        await stream.send_trailing_metadata(status=gui_grpc.grpclib.Status.PERMISSION_DENIED)
      except Exception as ex:
        print(ex)
        await stream.send_trailing_metadata(status=gui_grpc.grpclib.Status.UNKNOWN)
    
    async def laserGetState(self, stream) -> None:
      assert stream.metadata is not None
      metadata = {entry[0]: entry[1] for entry in stream.metadata.items() if isinstance(entry[1], str)}
      raw_request = await stream.recv_message()
      assert raw_request is not None
      request = Void.from_pb(raw_request)
      try:
        response = await self.impl.laserGetState(request = request, metadata = metadata)
        await stream.send_message(response.to_pb())
      except KeyboardInterrupt:
        raise
      except GrpcAccessDenied as ex:
        await stream.send_trailing_metadata(status=gui_grpc.grpclib.Status.PERMISSION_DENIED)
      except Exception as ex:
        print(ex)
        await stream.send_trailing_metadata(status=gui_grpc.grpclib.Status.UNKNOWN)
    
    async def laserSetState(self, stream) -> None:
      assert stream.metadata is not None
      metadata = {entry[0]: entry[1] for entry in stream.metadata.items() if isinstance(entry[1], str)}
      raw_request = await stream.recv_message()
      assert raw_request is not None
      request = LaserSetState.from_pb(raw_request)
      try:
        response = await self.impl.laserSetState(request = request, metadata = metadata)
        await stream.send_message(response.to_pb())
      except KeyboardInterrupt:
        raise
      except GrpcAccessDenied as ex:
        await stream.send_trailing_metadata(status=gui_grpc.grpclib.Status.PERMISSION_DENIED)
      except Exception as ex:
        print(ex)
        await stream.send_trailing_metadata(status=gui_grpc.grpclib.Status.UNKNOWN)
    
    async def laserSetRotationState(self, stream) -> None:
      assert stream.metadata is not None
      metadata = {entry[0]: entry[1] for entry in stream.metadata.items() if isinstance(entry[1], str)}
      raw_request = await stream.recv_message()
      assert raw_request is not None
      request = LaserSetRotationState.from_pb(raw_request)
      try:
        response = await self.impl.laserSetRotationState(request = request, metadata = metadata)
        await stream.send_message(response.to_pb())
      except KeyboardInterrupt:
        raise
      except GrpcAccessDenied as ex:
        await stream.send_trailing_metadata(status=gui_grpc.grpclib.Status.PERMISSION_DENIED)
      except Exception as ex:
        print(ex)
        await stream.send_trailing_metadata(status=gui_grpc.grpclib.Status.UNKNOWN)
    
    async def commSend(self, stream) -> None:
      assert stream.metadata is not None
      metadata = {entry[0]: entry[1] for entry in stream.metadata.items() if isinstance(entry[1], str)}
      raw_request = await stream.recv_message()
      assert raw_request is not None
      request = CommSend.from_pb(raw_request)
      try:
        response = await self.impl.commSend(request = request, metadata = metadata)
        await stream.send_message(response.to_pb())
      except KeyboardInterrupt:
        raise
      except GrpcAccessDenied as ex:
        await stream.send_trailing_metadata(status=gui_grpc.grpclib.Status.PERMISSION_DENIED)
      except Exception as ex:
        print(ex)
        await stream.send_trailing_metadata(status=gui_grpc.grpclib.Status.UNKNOWN)
    
    async def commReceive(self, stream) -> None:
      assert stream.metadata is not None
      metadata = {entry[0]: entry[1] for entry in stream.metadata.items() if isinstance(entry[1], str)}
      raw_request = await stream.recv_message()
      assert raw_request is not None
      request = CommReceiveRequest.from_pb(raw_request)
      try:
        response = await self.impl.commReceive(request = request, metadata = metadata)
        await stream.send_message(response.to_pb())
      except KeyboardInterrupt:
        raise
      except GrpcAccessDenied as ex:
        await stream.send_trailing_metadata(status=gui_grpc.grpclib.Status.PERMISSION_DENIED)
      except Exception as ex:
        print(ex)
        await stream.send_trailing_metadata(status=gui_grpc.grpclib.Status.UNKNOWN)
    
  
  def make_handler(self):
    return TheShipLowLevelInterface.TheShipLowLevelForwarder(self)
  

