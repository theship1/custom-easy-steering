import io
import asyncio
import json
from src import api_gui 
import grpclib.exceptions


async def read_message(reader: asyncio.StreamReader) -> dict:
    cmd = io.BytesIO()
    while True:
        b = await reader.readexactly(1)
        if b == b'\x00':
            break
        cmd.write(b)

    return json.loads(cmd.getvalue().decode('utf-8'))


async def write_message_raw(writer: asyncio.StreamWriter, message: bytes):
    writer.write(message)
    writer.write(b'\x00')
    await writer.drain()


async def write_message(writer: asyncio.StreamWriter, message: dict):
    await write_message_raw(writer, json.dumps(message).encode('utf-8'))


def resource_to_name(resource: api_gui.ResourceType):
    match resource:
        case resource.GOLD:
            return "Gold"
        case resource.PLATIN:
            return "Platin"
        case resource.IRON:
            return "Eisen"
        case _:
            assert False, "unknown resource"


def exception_to_json_error(func):
    from aiohttp import web

    async def wrapper(*args, **kwargs):
        try:
            return await func(*args, **kwargs)
        except Exception as ex:
            return web.json_response(status=500, text=json.dumps({
                "kind": "error",
            }))

    return wrapper


def simulation_retryable(func):

    async def wrapper(*args, **kwargs):
        retries = 0
        while True:
            if retries / 2 > 30:
                return await func(*args, **kwargs)
            else:
                try:
                    result = await func(*args, **kwargs)
                    retries = 0
                    return result
                except (grpclib.exceptions.StreamTerminatedError, ConnectionRefusedError) as ex:
                    print(f'connection to simulation lost ({ex}): retry after 2s')
                    await asyncio.sleep(2)
                    retries += 1

    return wrapper
