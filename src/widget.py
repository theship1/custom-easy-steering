#!/usr/bin/python3
#https://github.com/bazelbuild/bazel/issues/7091
#https://github.com/bazelbuild/rules_python/issues/382
import sys
if sys.path[0].startswith('/home/thingdust/src'):
    sys.path = sys.path[1:]

import asyncio
from util import read_message, write_message, write_message_raw
from typing import AsyncIterator, Callable, Awaitable, Tuple
from contextlib import asynccontextmanager
import os
import time
import json


async def _read_widget_messages(reader) -> AsyncIterator[dict]:
    while True:
        message = await read_message(reader)
        match message['kind']:
            case 'keepalive':
                pass
            case _:
                yield message


@asynccontextmanager
async def run_widget() -> AsyncIterator[Tuple[
    AsyncIterator[dict],
    Callable[[dict], Awaitable[None]],
    Callable[[dict], Awaitable[None]],
    Callable[[str], Awaitable[None]],
]]:
    timeout = time.monotonic() + 20
    while True:
        try:
            reader, writer = await asyncio.open_connection("192.168.0.3", 2002)
            break
        except OSError:
            if time.monotonic() > timeout:
                raise
            await asyncio.sleep(0.1)
    try:
        last_update_msg = None

        async def update_widget(widget_json):
            nonlocal last_update_msg
            update_msg = json.dumps({'kind': 'update_widget', 'widget': widget_json}).encode('utf-8')
            if last_update_msg != update_msg:
                await write_message_raw(writer, update_msg)
                last_update_msg = widget_json

        yield (
            _read_widget_messages(reader),
            update_widget,
            lambda cmd: write_message(writer, {
                'kind': 'command',
                'command': cmd
            }),
            lambda doc: write_message(writer, {
                'kind': 'update_doc',
                'doc': doc
            }),
        )
    finally:
        writer.close()
        await writer.wait_closed()
